\documentclass{../writeups}
\usepackage{graphicx}  % figures
\usepackage{amsmath,amsfonts,amssymb}  % equation environments
\usepackage{empheq}  % boxed equations
\usepackage{lmodern}  % need bold TT font


\title{How does \texttt{nbodies} produce nonlinear deflections in HAWC2?}
\author{Jenni Rinker}  % contributors
\date{\today}  % semester

\graphicspath{{./figures/}}


\begin{document}

\maketitle

When modelling modern wind turbine blades, which are slender and flexible, it is essential that the beam model accounts for nonlinear deflections.\footnote{This is one of the reasons you should take care when modelling large turbines in OpenFAST---significant discrepancies can arise from the linear beam model!}
The HAWC2 manual indicates that we need to use ``multiple bodies'' to produce nonlinear deflections in a blade, but what exactly does this mean?
And how does splitting a ``main body'' up into multiple bodies result in nonlinear deflections?
These are the questions we will focus on in this write-up.

\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multibody dynamics}

One of HAWC2's strengths (and arguably one of its weaknesses\footnote{One downside of multibody codes is that they are general and therefore hard optimize, so they will likely run slower than a program that assumes a specific formulation of the dynamical system like OpenFAST.}) is that it is a \textbf{multibody code}.
What this means is that the system is modelled as a series of bodies that are constrained together, and then the response of the coupled system is simulated.
Modelling complex dynamical systems using multibody frameworks is relatively common, and a very detailed treatment of the theory of general multibody dynamics can be found in \cite{Shabana2013}.
An in-depth description of HAWC2's multibody formulation is given in \cite{Goezcue2020}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Main bodies, \texttt{nbodies} and nonlinear deflections}

In HAWC2 we call continuous structural elements such as towers, blades, shafts, etc. \textbf{main bodies}. (See \cite{Larsen2015} for more info.)
Each main body is linked to a structural file that defines the distributed structural parameters such as Young's modulus, moments of inertia around different bending axes, etc.

Main bodies are modelled using discrete beam elements---Timoshenko beam elements, in fact.
Timoshenko beam theory assumes small deflections and therefore the deflections of the Timoshenko elemnts are linear.
In other words, if I double the force I have placed on a Timoshenko element, I will get twice as much deflection of the element.
This should set off warning bells for you!
If I have linear elements, how can possibly get nonlinear deflections in HAWC2!?
This is where the importance of \texttt{nbodies} and rotation matrices come in.

The trick to producing nonlinear deflections in a main body is to split the main body into a collection of \textbf{sub-bodies}, referred to as ``bodies'' in the HAWC2 manual.
The deflections within each sub-body are linear, but the deflections within the entire main body can be nonlinear.
(We'll explain how this works in detail in the next paragraph, don't worry!)
The number of sub-bodies is determined by the variable \textbf{nbodies} defined in the main-body block in the htc file, and HAWC2 splits up the main body so that roughly equal numbers of elements are in each sub-body.
An example is given in Fig.~\ref{fig:hawc2blade}, in which a main body with 18 Timoshenko beam elements is split into 6 sub-bodies, each with 3 elements.

% FIG. Blade1 diagram with elements and sub-bodies
\begin{figure}
	\centering
	\includegraphics[width=0.95\textwidth]{hawc2_blade}
	\caption{A main body called ``Blade1''.
		The main body is split into 18 elements with 19 nodes, and it has 6 bodies.
		Reprinted from HAWC2 v12.9 manual (p. 21).}
	\label{fig:hawc2blade}
\end{figure}

Now it's time to answer the real question: how does splitting a main body into sub-bodies result in nonlinear deflections.
The key concept is \textbf{rotated coordinate systems}.
Each sub-body has its own coordinate system that translates and rotates along with the node to which it is attached.
The linear deflections of the beam elements in that sub-body are then given with respect to its local coordinate system.

Let's show this with an example.
In Fig.~\ref{fig:beam_diagram} we have an inextensible cantilever beam with 4 elements.
The beam is fixed at the left-hand side and subject to a vertical force on the free end.
In the left plot we have one sub-body and therefore one coordinate system for all of the elements, which is indicated by the black arrows.
Because our beam elements are linear and the beam cannot deflect axially, the nodes in the beam can only deflect upwards or downwards with respect to the coordinate system; they cannot deflect laterally.
We can see therefore that the beam has deflected downwards but the width of the beam, measured horizontally, remains the same.


% FIG. Beam diagram with 2 bodies
\begin{figure}
	\centering
	\includegraphics[width=0.85\textwidth]{beam_diagram}
	\caption{A 4-element cantilever beam with one sub-body (left) and two sub-bodies (right).
		The undeflected positions are given in grey and the black arrows indicate the coordinate systems for the sub-bodies.
		Note the rotation of the coordinate system of the second sub-body in the right plot, which results in a shortening of the horizontal width of the beam.}
	\label{fig:beam_diagram}
\end{figure}

The right-hand plot, on the other hand, contains two sub-bodies.
The coordinate system for the second sub-body is fixed to node 3 and therefore translates and rotates with that node.
We can see therefore that the coordinate system of the second sub-body is shifted and rotated compared to the first coordinate system.
The deflections of the second sub-body can only be in the vertical direction with respect to their coordinate system, once again due to the beam theory assumptions, but now the coordinate system is rotated so we see a very different deflected shape of that sub-body.
In particular, for this example, the tip of the beam with 2 sub-bodies deflects less in the vertical direction but more in the horizontal direction compared to the beam with 1 sub-body.
We can therefore see that just by defining a new coordinate system for a second sub-body that we are better able to capture nonlinear deflected shapes.
Next up, let's demonstrate this with a cantilever beam with a tip load modelled in HAWC2.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{nbodies} in a cantilever beam}

We've learned a bit of the theory of why sub-bodies result in nonlinear deflections, but what does this look like in HAWC2?
What actually happens if we try to model a beam with one sub-body instead of multiple?
Let's find out.

Consider an inextensible, isotropic cantilever beam that has a point-load acting downwards at the tip.
The theoretical solution for the tip deflection of such a beam can be numerically solved for using the process detailed in Appendix~\ref{app:cantbeam}.
We will model this beam in HAWC2 and consider the deflected shapes for different numbers of sub-bodies when the deflections are (a) almost linear and (b) very nonlinear.
The beam we model in HAWC2 is made of 30 elements evenly distributed along the beam.


% --------------------------------------------------------
\subsection{Small deflections ($\approx$linear)}

In this case, we use a tip force $P=0.5EI/L^2$, where $P$ is the vertical force at the tip, $L$ is the length of the beam, and $EI$ is the flexural rigidity, defined as the product of the Young's modulus and the moment of inertia.
As can be seen in Appendix~\ref{app:cantbeam}, this loading is small enough to produce deflections that are close to the linear range.
Since these deflections are nearly linear, we don't expect to see much difference in the deflected shape if we vary \texttt{nbodies}.

The resulting deflected shapes for 1, 2, 8 and 15 sub-bodies are given in Fig.~\ref{fig:linear_deflections}.
There is little discernible difference between the deflected shapes for 2 or more sub-bodies.
However, we can see the difference in the horizontal width of the beam for a single sub-body compared to multiple sub-bodies.
This is expected from our discussion above: with only one sub-body and no axial deflection, the points can only translate vertically and therefore the horizontal width of the beam remains unchanged.
But the difference between the single-body and multi-body deflections is relatively small, so we could conclude that, for this value of $P$, modelling the beam with linear deflections is perhaps acceptable.


% FIG. Small deflections
\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.9\textwidth]{linear_deflections}
	\caption{Deflected shapes of cantilever beams subject to a tip load of $P=0.5EI/L^2$, simulated in HAWC2 with different numbers of sub-bodies.
		The theoretical tip deflection is indicated by the red point and the superimposed axes are a magnification of the tip deflections.}
	\label{fig:linear_deflections}
\end{figure}

% --------------------------------------------------------
\subsection{Large deflections (nonlinear)}

Let's now consider a substantially larger loading, $P=7EI/L^2$, which should be well in the nonlinear region of beam deflections.
The resulting deflected shapes with the different numbers of sub-bodies are visualized in Fig.~\ref{fig:nonlinear_deflections}.

\textit{Now} we can really see how far off the result with a single sub-body is: the tip deflects almost 2.5 times more than the initial length of the beam!
Another interesting observation is that we can clearly see the point in the 2-body shape where the coordinate system rotates, as the second half of the beam deflects very differently from the first half.
The difference between 8 and 15 sub-bodies is relatively small and they both match the theoretical tip deflection relatively well, so we could probably get away with only using 8 sub-bodies for this simulation.


% FIG. Large deflections
\begin{figure}[ht!]
	\centering
	\begin{minipage}{0.4\textwidth}
		\includegraphics[height=0.5\textheight]{nonlinear_deflections}
	\end{minipage}
	\begin{minipage}{0.55\textwidth}
		\caption{Deflected shapes of cantilever beams subject to a tip load of $P=7EI/L^2$, simulated in HAWC2 with different numbers of sub-bodies.
		The theoretical tip deflection is indicated by the red point, and the superimposed axes are a magnification of the deflections.
		Take special note of the tip deflection with 1 sub-body, it is almost 2.5 times longer than the initial length of the beam!}
		\label{fig:nonlinear_deflections}
	\end{minipage}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Impact on aeroelastic simulations}

The final thing to discuss is the ramifications of different values of \texttt{nbodies} on wind turbine simulations.
Luckily, there is an excellent publication by my colleagues Ozan and David \cite{Goezcue2020} who did a deep dive into the impact of \texttt{nbodies} on convergence times and aeroelastic results.
The paper has a lot of interesting conclusions, but I will discuss only two of them in detail.

First, David and Ozan investigated the impact of \texttt{nbodies} for two different turbines: the IEA 10 MW and the DTU 10 MW.
In the paper there is consistently slower convergence rates of the IEA 10 MW compared to the DTU 10 MW when considering the number of sub-bodies.
This makes perfect sense, as the IEA 10 MW is more flexible than the DTU 10 MW and has more prebend, so we expect nonlinear deflections to play a larger role.
Thus, we will generally need a larger value of \texttt{nbodies} for the IEA 10 MW to achieve the same accuracy as the DTU 10 MW.

Second, there are clear, quantifiable errors in the results if nonlinear deflections are not included.
For example, the IEA 10 MW tower clearance is off by a whopping \textit{80\%} if you assume linear deflections for DLC 1.1.
This has huge ramifications for blade design!
The fatigue loads for DLC 1.2 are off by about 20\% for DLC 1.2, which is also quite substantial.
The convergence rate for the fatigue damage-equivalent loads (DELs) from the paper are reprinted in Fig.~\ref{fig:ozandavid}.
You can see that at least 12 sub-bodies are needed in the IEA 10 MW to get results within 1\% of the final DEL value, whereas the DTU 10 MW has okay accuracy after only 9 sub-bodies.
This highlights the importance of convergence studies on how many sub-bodies you need for your wind turbine model!
Furthermore, for very large turbines (10 MW and larger), you might even need multiple bodies on your tower to get proper results.
Simulating with nonlinear tower deflections is not common practice yet, but it's something you should be aware of when simulating large and flexible turbines.


% FIG. Beam diagram with 2 bodies
\begin{figure}
	\centering
	\includegraphics[width=0.95\textwidth]{f07}
	\caption{Figure 7 from \cite{Goezcue2020} reprinted with permission.
		Both turbine models modelled the blades using 30 elements.
		The higher flexibility and larger prebend of the IEA 10 MW results in a slower convergence of the fatigue loads.}
	\label{fig:ozandavid}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
\section{Conclusion}

Even cantilever beams with a relatively light load at the tip ($P=0.5EI/L^2$) have a noticeable discrepancy in the tip deflection when using linear versus nonlinear beam theory.
And of course this discrepancy increases as you increase the loading on the beam.
For large and flexible wind turbines, simulations that don't take nonlinear deflections into account can produce differences in aeroelastic behavior and therefore loads.
So be sure to do a convergence study on \texttt{nbodies} for both blades and towers if you're modelling wind turbines with HAWC2!
Not being aware of the effect of this parameter can produce significant modelling errors in your results.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONTRIBUTORS
%\clearpage
%\section*{Contributors}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFERENCES
\bibliographystyle{ieeetranN}
%\bibliography{C:/Users/rink/Dropbox/Papers/Master}
\bibliography{nbodies_nonlinear}







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% APPENDICES
\appendix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Theoretical tip deflection of a cantilever beam}
\label{app:cantbeam}

Some smart people \cite{Bisshopp1945} have used fancy math to determine the analytical tip deflection and translation of an Euler-Bernoulli beam subject to a tip force.
Because the math involves elliptic integrals, the resulting equations need to be solved numerically.
But hey, this is 2022 and we can do that easily.
Thanks, Python!

If we define the vertical displacement of the beam as $\delta$ and the horizontal displacement of the beam as $\Delta$, then the parametric equations governing the tip displacement are given by (Eqs. 9, 7, 8, 5, 10, and 6 in Bisshopp and Drucker)
\begin{eqnarray}
	\frac{\delta}{L} &=& 1 - \frac{2}{\alpha}\left[E(k)-E(k, \theta_1)\right] \\
	\theta_1 &=& \sin^{-1}\left(\frac{\sqrt{2}}{2k}\right) \\
	\alpha &=& F(k) - F(k, \theta_1) \\
	\alpha^2 &=& \frac{PL^2}{EI} \\
	\phi_0 &=& \sin^{-1}(2k^2-1) \\
	\frac{L-\Delta}{L} &=& \frac{\sqrt{2}}{\alpha}\sqrt{\sin\phi_0}
\end{eqnarray}
where $E(k)$ is the complete elliptic integral of the second kind, $E(k, \phi)$ is the incomplete elliptic integral of the second kind, $F(k)$ is the complete elliptic integral of the first kind, $F(k, \phi)$ is the incomplete elliptic integral of the first kind, and $k$ is the parametric variable that satisfies $k^2 \in (0.5, 1)$.

If we evaluate these equations numerically, we get the curves plotted in Fig.~\ref{fig:beam_theory}, which luckily match Fig.~1 in Bisshopp and Drucker.
Whew!
We can estimate the linear region by fitting a line to the first few points of $\delta/L$ and extrapolating outwards to see where $\delta/L$ diverges.
It looks like the vertical deflections are roughly linear until $\alpha^2\approx 0.6$.
The discrepancies in the axial deflection are expected more significant, even for small $\alpha$.

% FIG. Large deflections
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{beam_theory}
	\caption{Parametric curves for tip deflection and translation, calculated using the equations from \cite{Bisshopp1945}.
		Red points indicate $\alpha^2=7$ (nonlinear) and green points indicate $\alpha^2=0.5$ (linear).
	}
	\label{fig:beam_theory}
\end{figure}

If we use these numeric curves to interpolate the locations of the tip of the beam for $\alpha^2=0.5$ (green points) and $\alpha^2=7$ (red points), which are the two load values we investigate in this write-up, we get the following numbers:
\begin{center}
	\begin{tabular}{r|lcc}
		$\alpha^2$ & Deflection & $\delta/L$ & $(L-\Delta)/L$ \\\hline
		0.5 & Linear & 0.162 & 0.984 \\
		7 & Nonlinear & 0.766 &  0.529
	\end{tabular}
\end{center}


\end{document}

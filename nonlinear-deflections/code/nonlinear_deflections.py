# -*- coding: utf-8 -*-
"""Code to make the nonlinear deflections
"""
from wetb.hawc2.Hawc2io import ReadHawc2
import matplotlib.pyplot as plt
import numpy as np

E = 2.1000E+5
I = 1
L = 1


nsec = 31
secs = np.linspace(0, L, nsec)
beams = [1, 2, 8, 15]

# print('\n')
# print(f'        nsec {nsec}')
# for i, sec in enumerate(secs):
#     print(f'        sec {i+1} 0 0 {-sec:.3f} 0 ;')
    
# print('\n')
# for ibeam in beams:
#     for ielem in range(1, nsec):
#         print(f'    mbdy state	pos beam{ibeam} {ielem} 1 global # beam{ibeam} {ielem+1} pos;')

#%% make plots NONLINEAR

alpha2 = 7


B = E * I
P = alpha2 * B / L**2
print(P)

delta = 0.776 * L
Delta = L - 0.529 * L
delta_G = 0 - delta
Delta_G = L - Delta
print(delta_G)
print(Delta_G)

path, fignum = './res/beam', 1
data = ReadHawc2(path).ReadGtsdf()

steady_state = data[-1, 1:]
steady_y = np.zeros((len(beams), nsec))
steady_z = np.zeros((len(beams), nsec))
steady_y[:, 1:] = steady_state[1::3].reshape(len(beams), nsec-1)
steady_z[:, 1:] = steady_state[2::3].reshape(len(beams), nsec-1)

linestyles = [':', '-.', '--', '-']
markers = ['x', '^', 's', 'o']
colors = ['0.5', '0.5', '0.45', '0.4']

fig, ax = plt.subplots(num=fignum, clear=True, figsize=(4, 8))
axz = fig.add_axes([0.23, 0.2, 0.4, 0.4])

ax.plot(secs, np.zeros(nsec), '-o', ms=3, c='0.3', alpha=0.2)
axz.plot(secs, np.zeros(nsec), '-o', ms=3, c='0.3', alpha=0.2)
for i, ibeam in enumerate(beams):
    ax.plot(steady_y[i], -steady_z[i], ms=3, label=f'{ibeam} bodies',
            linestyle=linestyles[i], marker=markers[i], c=colors[i])
    if i > 0:
        axz.plot(steady_y[i], -steady_z[i], ms=3,
                 linestyle=linestyles[i], marker=markers[i], c=colors[i])
ax.plot(Delta_G, delta_G, 'o', c='C3', ms=6, alpha=0.8, zorder=-1, 
        label='Analytical tip location')
axz.plot(Delta_G, delta_G, 'o', c='C3', ms=6, alpha=0.8, zorder=-1)
ax.axis('equal')
ax.set(ylabel='Beam deflection [m]')
ax.grid()
axz.grid()
ax.legend()
axz.set(xticklabels=[], yticklabels=[], xlim=[0.4, 0.7], ylim=[-0.9, -0.5])

fig.tight_layout()
fig.savefig('../figures/nonlinear_deflections.png', dpi=150)

#%% make plots LINEAR

alpha2 = 0.5


B = E * I
P = alpha2 * B / L**2

delta = 0.162 * L
Delta = L - 0.984 * L
delta_G = 0 - delta
Delta_G = L - Delta

# path, fignum = './res/beam', 1
path, fignum = './res/beam_linear', 2
data = ReadHawc2(path).ReadGtsdf()

steady_state = data[-1, 1:]
steady_y = np.zeros((len(beams), nsec))
steady_z = np.zeros((len(beams), nsec))
steady_y[:, 1:] = steady_state[1::3].reshape(len(beams), nsec-1)
steady_z[:, 1:] = steady_state[2::3].reshape(len(beams), nsec-1)


fig, ax = plt.subplots(num=fignum, clear=True, figsize=(7, 3.5))
axz = fig.add_axes([0.15, 0.14, 0.45, 0.35])


ax.plot(secs, np.zeros(nsec), '-o', ms=3, c='0.3', alpha=0.2)
axz.plot(secs, np.zeros(nsec), '-o', ms=3, c='0.3', alpha=0.2)
for i, ibeam in enumerate(beams):
    ax.plot(steady_y[i], -steady_z[i], ms=3, label=f'{ibeam} bodies',
            linestyle=linestyles[i], marker=markers[i], c=colors[i])
    if i > 0:
        axz.plot(steady_y[i], -steady_z[i], ms=3,
                 linestyle=linestyles[i], marker=markers[i], c=colors[i])
ax.plot(Delta_G, delta_G, 'o', c='C3', ms=6, alpha=0.8, zorder=-1, 
        label='Analytical tip location')
axz.plot(Delta_G, delta_G, 'o', c='C3', ms=6, alpha=0.8, zorder=-1)
ax.axis('equal')
ax.set(xlim=[0, 1.05], ylim=[-0.3, 0], ylabel='Beam deflection [m]')
ax.grid()
axz.grid()
axz.set(xlim=[0.92, 1.001], ylim=[-0.17, -0.14], xticklabels=[], yticklabels=[], )
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
          ncol=3, mode="expand", borderaxespad=0)

fig.tight_layout()
fig.savefig('../figures/linear_deflections.png', dpi=150)

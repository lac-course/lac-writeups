# -*- coding: utf-8 -*-
"""Linear versus nonlinear
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline

x = np.linspace(0, 1, 5)
y_undef = np.zeros_like(x)
m = 0.3
y_linear = -m * x

ylim = [-0.75, 0.2]


fig, (ax0, ax1) = plt.subplots(1, 2, num=20, figsize=(8, 3), clear=True)

#%% linear

ax0.cla()

# undeflected position
ax0.plot(x, y_undef, 'o-', c='0.5', alpha=0.5, zorder=1)

# coordinate systems
for i in [0]:
    ax0.annotate('', xy=(x[i], y_linear[i]), xytext=(x[i] + 0.15, y_linear[i]),
                 arrowprops=dict(arrowstyle="<-"), xycoords='data', textcoords='data')
    ax0.annotate('', xy=(x[i], y_linear[i]), xytext=(x[i], y_linear[i] + 0.15),
                 arrowprops=dict(arrowstyle="<-"), xycoords='data', textcoords='data')

# make spline interpolator
x_spline_l = np.array([-0.05, 0.05, 1])
y_spline_l = np.array([0, 0, -0.6])
cs_l = CubicSpline(x_spline_l, y_spline_l)
xplot_l = np.linspace(0, 1)
yplot_l = cs_l(xplot_l)

# deflected positions
ax0.plot(xplot_l, yplot_l, c='C0', alpha=0.8)
ax0.plot(x, cs_l(x), 'o', c='C0', alpha=0.8)
ax0.grid()
ax0.set(ylim=ylim, xticklabels=[], yticklabels=[], title='4 elements, 1 body')
ax0.tick_params(length=0)
ax0.axis('equal')

#%% nonlinear

ax1.cla()

# undeflected position
ax1.plot(x, y_undef, 'o-', c='0.5', alpha=0.5, zorder=1)

# make spline interpolator for first body
x_spline1 = np.array([-0.05, 0.05, 0.5])
y_spline1 = np.array([0, 0, -0.09])
cs1 = CubicSpline(x_spline1, y_spline1)
xplot1 = np.linspace(0, 0.5)
yplot1 = cs1(xplot1)

# identify the location/rotation of node 3 and rotation matrix
x3 = xplot1[-1]
y3 = yplot1[-1]
m = (yplot1[-1] - yplot1[-2]) / (xplot1[-1] - xplot1[-2])
theta = np.arcsin(m)
print(theta*180/np.pi)
rot_mat = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])

# make spline interpolator for second body in local coordinate system
x_spline2_r = np.array([-0.025, 0.025, 0.5])
y_spline2_r = np.array([0, 0, -0.15])
cs2 = CubicSpline(x_spline2_r, y_spline2_r)
x2_r = np.linspace(0, 0.5)
y2_r = cs2(xplot1)
x2_p_r = np.array([0.25, 0.5])
y2_p_r = cs2(x2_p_r)

# transform second body to non-rotated coordinate system
local_rot = rot_mat @ np.vstack((x2_r, y2_r))
xplot2 = x3 + local_rot[0]
yplot2 = y3  + local_rot[1]
local_rot = rot_mat @ np.vstack((x2_p_r, y2_p_r))
xplot2_p = x3 + local_rot[0]
yplot2_p = y3  + local_rot[1]

# undeflected second body
x_line, y_line = np.empty(3), np.empty(3)
x_line[0], y_line[0] = x[2], y3
x_local = x[3:] - x[2]
y_local = y_undef[3:] - y_undef[2]
local_rot = rot_mat @ np.vstack((x_local, y_local))
x_line[1:] = x3 + local_rot[0]
y_line[1:] = y3 + local_rot[1]
ax1.plot(x_line, y_line, 'o-', c='0.5', alpha=0.5, zorder=1)

# assemble points for plotting
x_nonlinear = np.empty_like(x)
y_nonlinear = np.empty_like(x)
x_nonlinear[:3] = x[:3]
y_nonlinear[:3] = cs1(x[:3])
x_nonlinear[3:] = xplot2_p
y_nonlinear[3:] = yplot2_p


# coordinate systems
for i in [0, 1]:
    if i:
        xtip1, ytip1 = rot_mat @ [0.15, 0]
        xtip2, ytip2 = rot_mat @ [0, 0.15]
    else:
        xtip1, ytip1 = [0.15, 0]
        xtip2, ytip2 = [0, 0.15]
    xplot_c = [x[0], x3][i]
    yplot_c = [y_undef[0], y3][i]
    ax1.annotate('', xy=(xplot_c, yplot_c), xytext=(xplot_c + xtip1, yplot_c + ytip1),
                 arrowprops=dict(arrowstyle="<-"), xycoords='data', textcoords='data')
    ax1.annotate('', xy=(xplot_c, yplot_c), xytext=(xplot_c + xtip2, yplot_c + ytip2),
                 arrowprops=dict(arrowstyle="<-"), xycoords='data', textcoords='data')


# deflected positions
ax1.plot(xplot1, yplot1, c='C2', alpha=0.8)
ax1.plot(xplot2, yplot2, c='C2', alpha=0.8)
ax1.plot(x_nonlinear, y_nonlinear, 'o', c='C2', alpha=0.8)
ax1.grid()
ax1.axis('equal')
ax1.set(ylim=ylim, xticklabels=[], yticklabels=[], title='4 elements, 2 bodies')
ax1.tick_params(length=0)

fig.tight_layout()
fig.savefig('../figures/beam_diagram.png', dpi=150)

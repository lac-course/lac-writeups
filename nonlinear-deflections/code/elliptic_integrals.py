# -*- coding: utf-8 -*-
"""
Large deflections of cantilever beams, Bisshop and Drucker 1945
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import ellipk, ellipkinc, ellipe, ellipeinc


# define our range of m = k^2
k2 = np.linspace(0.5, 1)[1:-1]

# Evaluate vertical displacement
theta1 = np.arcsin(np.sqrt(2) / 2 / np.sqrt(k2)) # Eq. 7
alpha = ellipk(k2) - ellipkinc(theta1, k2)  # Eq. 8
delta = 1 - 2/alpha * (ellipe(k2) - ellipeinc(theta1, k2))  # Eq. 9

# Evaluate horisontal displacement
theta0 = np.arcsin(2*k2 - 1)  # Eq. 10
Delta = np.sqrt(2) / alpha * np.sqrt(np.sin(theta0))  # Eq. 10

# Evaluate two specific points
alpha1 = np.sqrt(7)
delta1 = np.interp(alpha1, alpha, delta)
Delta1 = np.interp(alpha1, alpha, Delta)
print(f'\nFor PL^2/B = {alpha1**2:.1f}:')
print(f'delta/L = {delta1:.3f}')
print(f'(L - Delta)/Delta = {Delta1:.3f}')

alpha2 = np.sqrt(0.5)
delta2 = np.interp(alpha2, alpha, delta)
Delta2 = np.interp(alpha2, alpha, Delta)
print(f'\nFor PL^2/B = {alpha2**2:.1f}:')
print(f'delta/L = {delta2:.3f}')
print(f'(L - Delta)/Delta = {Delta2:.3f}')

# fit a line to the first points
xlinear = np.linspace(0, 1)
slope = (alpha[1]**2 - alpha[0]**2) / (delta[1] - delta[0])
ylinear = slope * xlinear

# Recreate figure 1 with our two points
fig, ax = plt.subplots(num=10, clear=True, figsize=(5, 4))
ax.plot(delta, alpha**2, '--', c='0.2', lw=2, label=r'$\delta/L$')
ax.plot(Delta, alpha**2, c='0.4', lw=2, label=r'$(L - \Delta)/L$')
ax.plot(xlinear, ylinear, c='0.3', lw=1, label='Linear beam theory')
ax.plot(delta1, alpha1**2, 'o', c='C3', ms=6, alpha=0.7, label=r'$PL^2/B=7$')
ax.plot(Delta1, alpha1**2, 'o', c='C3', ms=6, alpha=0.7)
ax.plot(delta2, alpha2**2, 'o', c='C2', ms=6, alpha=0.7, label=r'$PL^2/B=0.5$')
ax.plot(Delta2, alpha2**2, 'o', c='C2', ms=6, alpha=0.7)
ax.plot()
ax.grid()
ax.set(xlim=[0, 1], ylim=[0, 8],
       xlabel='Normalized displacement [-]',
       ylabel=r'$\alpha^2 = PL^2/B$')
ax.legend()
fig.tight_layout()

fig.savefig('../figures/beam_theory.png', dpi=150)
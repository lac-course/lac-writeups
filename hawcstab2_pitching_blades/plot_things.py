# -*- coding: utf-8 -*-
"""Why is HAWCStab2 pitching my blades before I reach rated power or torque?
"""
import matplotlib.pyplot as plt
import numpy as np

pmax = 10641.618
omega_max = 9.6 * np.pi / 30

powkw = dict(marker='.', alpha=0.5, c='C3', lw=2)
refkw = dict(linestyle='-.', c='0.2', zorder=-1)

fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(8, 5), clear=True, num=1)

#%% TSR of 7.5

wsp, pit, rpm, pwr, thr = np.loadtxt('DTU_10MW_hawc2s_rigid_7.5.opt', skiprows=1,
                                     unpack=True)

omega = rpm * np.pi / 30
torque = pwr / omega
pref = omega * pmax / omega_max


ax0.plot(wsp, pwr, label='Steady-state', **powkw)
ax0.plot(wsp, pref, label='Maximum', **refkw)
ax0.set(ylabel='Aerodynamic power [kW]', title='Rigid, TSR = 7.5')
ax0.grid()

#%% TSR of 7.9

wsp, pit, rpm, pwr, thr = np.loadtxt('DTU_10MW_hawc2s_rigid_7.9.opt', skiprows=1,
                                     unpack=True)

omega = rpm * np.pi / 30
torque = pwr / omega
pref = omega * pmax / omega_max


ax1.plot(wsp, pwr, label='Steady-state', **powkw)
ax1.plot(wsp, pref, label='Maximum', **refkw)
ax1.set(ylabel='Aerodynamic power [kW]', title='Rigid, TSR = 7.9',
        xlabel='Wind speed [m/s]')
ax1.grid()

fig.tight_layout()
fig.savefig('power_reference.png', dpi=150)
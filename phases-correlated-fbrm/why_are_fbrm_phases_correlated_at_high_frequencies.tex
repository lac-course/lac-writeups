\documentclass{../writeups}
\usepackage{graphicx}  % figures
\usepackage{amsmath,amsfonts,amssymb}  % equation environments
\usepackage{empheq}  % boxed equations
\usepackage{lmodern}  % need bold TT font


\title{Why are the Fourier phases of the flapwise bending moment in a straight line at high frequencies?}
\author{Jenni Rinker}  % contributors
\date{\today}  % semester

%\graphicspath{{./figures/running_many_hawc2_simulations/}}


\begin{document}


\maketitle
%\tableofcontents

%Trees live in your fan brush, but you have to scare them out. We'll play with clouds today. You need to have a very firm paint to do this. Nice little clouds playing around in the sky.

We often look at the Fourier transform of loads signals to identify what frequencies are in the signal.
This is useful if we are trying to, for example, identify resonant frequencies, validate natural frequencies of our model with data, try to identify rotational frequencies in data, etc.

Most of the time we consider only the magnitude of the Fourier vector, which tells us the amplitudes of the sinusoids that make up the overall signal.
Very rarely do we consider the phase content, which reflects the time-shift of each sinusoidal component.
If we take a moment to look at the phase content, we might see something quite interesting, maybe even unexpected...

As an example, consider the flapwise blade-root moment (BRM) of the DTU 10 MW.
The time history of the hub-height wind speed and the load response are plotted in Fig.~\ref{fig:fbrm_time} below.
We see a normal flapwise BRM response, with the largest oscillations happening at 1P.

% FIG. Wind, FBRM time series.
\begin{figure}[h]\centering
	\includegraphics[height=8cm]{fbrm_wind_time}
	\caption{Response of DTU 10 MW flapwise BRM to turbulent wind with a mean of 11~m/s.}
	\label{fig:fbrm_time}
\end{figure}

Now what do we see when we consider the magnitude and phase of the Fourier vector? \footnote{Note!
Taking the Fourier transform of a signal without any windowing or averaging is generally not recommended.
A Fourier transform assumes the signal is periodic, which real signals generally aren't.
Thus, weird things happen in the Fourier domain when this is not taken into account...but I'm getting ahead of myself. ;)
}
We've visualized that information in Fig.~\ref{fig:fbrm_bode} below.
The Fourier magnitude, plotted in the top axes, looks as expected: the lower frequencies have the highest energy, and there is a peak at 1P.
(This peak is difficult to see because the plot shows the full frequency band, but trust me! It's there.)
The Fourier phases are shown in the bottom plot, and this is where we see some interesting behaviour.
In particular, above roughly 20~Hz, there is a noticeable linear trend in the phases.
In other words, above a certain frequency, the phases are clearly \emph{correlated}.
This begs the question: why?
What is causing these phases to match each other at the high frequencies?

% FIG. Bode plot.
\begin{figure}[h]\centering
	\includegraphics[height=7cm]{fbrm_bode_plot}
	\caption{Magnitude (top) and phase (bottom) of the Fourier vector for the flapwise BRM.
		The region of correlated phases is indicated by the pink region.}
	\label{fig:fbrm_bode}
\end{figure}

To answer this, we will use a trick suggested to me by my colleague Fabio.
Make a copy of the original Fourier vector and set everything below 20~Hz to zero---in other words, isolate the high-frequency content.
Taking the inverse Fourier transform of this new Fourier vector should result in a time series that reveals why this phase correlation exists.

The results are plotted in Fig.~\ref{fig:fbrm_hf} below, with the original signal on top and the isolated high-frequency signal below.
The high-frequency content shows clear ``spikes'' at the beginning and end of the time series, and at first glange you might think they correspond to the two large excursions in the original signal also happening at the beginning and end of the signal.
To verify this, let's zoom into the end and beginning of the signal and wrap the signal in the time domain, since the Fourier transform is periodic.
In other words, plot the responses (both original and high-frequency only) from 699 to 700~s, followed by 100 to 101~s.
The result is in Fig.~\ref{fig:fbrm_hf_periodic}.


% FIG. High-frequency BRM.
\begin{figure}[h]\centering
	\includegraphics[height=8cm]{fbrm_hf}
	\caption{Original flapwise BRM (top) and isolated high-frequency content (bottom).}
	\label{fig:fbrm_hf}
\end{figure}

% FIG. High-frequency BRM with repeat.
\begin{figure}[h]\centering
	\includegraphics[height=8cm]{fbrm_wind_time_repeat}
	\caption{Original flapwise BRM (top) and isolated high-frequency content (bottom) assuming signal is periodic in time.}
	\label{fig:fbrm_hf_periodic}
\end{figure}


Now we see very clearly that there is a discontinuous jump in our otherwise smooth signal at the edge of the signal, which is precisely what is causing the spike in the high-frequency content.
Obviously we should pay more attention to signals processing!
To remove this discontinuity, apply a Tukey window\footnote{
A Tukey window is not necessarily the best window to use here, but it is okay to use because it preserves amplitudes of transient events.
There is a nice summary of different windows and their uses on this forum: \url{https://community.sw.siemens.com/s/article/window-types-hanning-flattop-uniform-tukey-and-exponential}.
Note that I'm also neglecting spectral averaging, which is not good practice but is okay for what we're trying to show here.
}
with 1\% of the signal in the tapered cosine region to the flapwise BRM signal with the mean removed.
The results are shown in Fig.~\ref{fig:fbrm_bode_tukey} below.


% FIG. Bode plot.
\begin{figure}[h]\centering
	\includegraphics[width=0.9\textwidth]{fbrm_bode_plot_tukey}
	\caption{Top: time signal of flapwise BRM with mean value removed and Tukey window applied.
		Middle: Magnitude of Fourier vector.
		Bottom: Phase of Fourier vector.}
	\label{fig:fbrm_bode_tukey}
\end{figure}

Applying the Tukey window appears to have little to no impact on the magnitude of the Fourier vector, but we clearly see now that the phases are completely uncorrelated.
So, there we have it folks: if we apply an FFT to a signal with no windowing, we will likely get high-frequency phase correlation due to the signal not being periodic.

\textbf{Bottom line}: It may look like the high-frequency phases of the flapwise blade root moment are correlated, but they're not!
Applying a window function reveals the phase correlation is due to the signal not being periodic.

% TABLE. System states and parameters
%\begin{table}\centering
%	\caption{Summary of system states and parameters for Ronnie.}
%	\begin{tabular}{rcl}
%	Symbol & Value & Meaning \\\hline
%	$k_t$ & 11,369,784~N/rad & Torsional spring constant for blades\\
%	$\omega_0$ & 0.8~Hz & Blade edgewise natural frequency\\
%	$k_x$ & 200,000~N/m & Lateral spring constant for $M$\\
%	$k_y$ & - & Vertical spring constant for $M$\\
%	$l$ & 30~m & Distance between $m$ and $M$\\
%	$m$ & 500~kg & Mass of each rotating mass\\
%	$M$ & 50,000~kg & Mass of center mass\\
%	$\Omega$ & - & Speed of rotating system (rad/s)\\
%	$\Psi_i$ & - & Azimuthal angle of mass $i$\\
%	$\theta_i$ & - & Angular deflection of mass $i$\\
%	$x$ & - & Lateral translation of $M$\\
%	$y$ & - & Vertical translation of $M$\\
%	\end{tabular}
%	\label{tab:system}
%\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overview
%\clearpage
%\section{Section title}


% --------------------------------------------------------
%\subsection{Subsection title}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONTRIBUTORS
%\clearpage
%\section*{Contributors}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFERENCES
%\bibliographystyle{ieeetranN}
%\bibliography{C:/Users/rink/Dropbox/Papers/Master}
%\bibliography{running_many_hawc2_simulations}







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% APPENDICES
%\appendix
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%\section{LTI modal analysis review}
%\label{app:modalreview}




\end{document}
# -*- coding: utf-8 -*-
"""Code to create figures for handout -- phases correlated high frequency.
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal.windows import tukey
from wetb.hawc2.Hawc2io import ReadHawc2

file = 'dtu_10mw_rwt_turb_11.0_1633.sel'

h2f = ReadHawc2(file)
data = h2f.ReadAll()

iwind = 15 - 1
ifbm = 28 - 1
iom = 10 - 1

time = data[:, 0]
wind = data[:, iwind]
omega = data[:, iom]
fbm = data[:, ifbm] / 1e3  # MNm


#%% Plot the time series


fig, (ax0, ax1) = plt.subplots(2, 1, num=1, clear=True, figsize=(8, 5))

ax0.plot(time, wind)
ax0.set(xlim=[100, 700], ylabel='Hub-height wind [m/s]')
ax1.plot(time, fbm)
ax1.set(xlim=[100, 700], xlabel='Time [s]', ylabel='Flapwise BRM [MNm]')

fig.tight_layout()

fig.savefig('../fbrm_wind_time.png', dpi=150)

#%% Plot the bode plot

x = fbm
X = np.fft.rfft(x)
f = np.fft.rfftfreq(time.size, 0.01)

oneP = 9.6 / 60
ylo, yhi = [1e-2, 1e6]

fig, (ax0, ax1) = plt.subplots(2, 1, num=2, clear=True, figsize=(8, 4.5))

ax0.semilogy(f, np.abs(X), lw=1)
ax0.semilogy([oneP, oneP], [ylo, yhi], '--', c='0.3', alpha=0.5, lw=1, zorder=-4)
ax0.set(xlim=[0, 50], ylabel='Magnitude of X(f) [MNm]', ylim=[ylo, yhi])
ax1.plot(f, np.angle(X)*180/np.pi, 'o', ms=1, alpha=0.7)
ax1.fill_between([20, 50], [-180, -180], [180, 180], facecolor='r', alpha=0.2, zorder=-5)
ax1.set(xlim=[0, 50], ylabel='Phase of X(f) [deg]', ylim=[-180, 180], xlabel='Frequency [Hz]')
ax1.text(25, 70, 'Correlated phases?', fontsize=14, color='0.25')

fig.tight_layout()

fig.savefig('../fbrm_bode_plot.png', dpi=150)

#%% isolate the high frequency content, take IFFT

fc = 20
Xhf = X.copy()
Xhf[f < fc] = 0
Xhf[0] = X[0]

fbm_hf = np.fft.irfft(Xhf, time.size)

fig, (ax0, ax1) = plt.subplots(2, 1, num=3, clear=True, figsize=(8, 5))

ax0.plot(time, x)
ax0.set(xlim=[90, 710], ylabel='Flapwise BRM [MNm]')
ax1.plot(time, fbm_hf)
ax1.set(xlim=[90, 710], xlabel='Time [s]', ylabel='High-frequency content [MNm]')

fig.tight_layout()

fig.savefig('../fbrm_hf.png', dpi=150)


#%% Plot the repeating time series

fig, (ax0, ax1) = plt.subplots(2, 1, num=11, clear=True, figsize=(8, 5))

npts = 100
repeat = np.concatenate((fbm[-npts:], fbm[:npts]))
time_repeat = np.concatenate((time[-npts:], time[:npts] + 600))
repeat_hf = np.concatenate((fbm_hf[-npts:], fbm_hf[:npts]))

ax0.plot(time_repeat, repeat)
ax0.set(xlim=[699, 701], ylabel='Flapwise BRM [MNm]')
ax1.plot(time_repeat, repeat_hf)
ax1.set(xlim=[699, 701], xlabel='Time [s]', ylabel='High-frequency content [MNm]')

fig.tight_layout()

fig.savefig('../fbrm_wind_time_repeat.png', dpi=150)

#%% Applya tukey window

window = tukey(time.size, alpha=0.01)
x = (fbm - fbm.mean()) * window
X = np.fft.rfft(x)
f = np.fft.rfftfreq(time.size, 0.01)

oneP = 9.6 / 60
ylo, yhi = [1e-3, 1e6]

fig, (axn1, ax0, ax1) = plt.subplots(3, 1, num=13, clear=True, figsize=(8, 6.5))

axn1.plot(time, x)
axn1.set(xlabel='Time [s]', ylabel='Windowed signal', xlim=[100, 700])
ax0.semilogy(f, np.abs(X), lw=1)
ax0.semilogy([oneP, oneP], [ylo, yhi], '--', c='0.3', alpha=0.5, lw=1, zorder=-4)
ax0.set(xlim=[0, 50], ylabel='Magnitude of X(f) [MNm]', ylim=[ylo, yhi], xlabel='Frequency [Hz]')
ax1.plot(f, np.angle(X)*180/np.pi, 'o', ms=1, alpha=0.7)
ax1.set(xlim=[0, 50], ylabel='Phase of X(f) [deg]', ylim=[-180, 180], xlabel='Frequency [Hz]')

fig.tight_layout()

fig.savefig('../fbrm_bode_plot_tukey.png', dpi=150)
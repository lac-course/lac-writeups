# -*- coding: utf-8 -*-
"""Demonstrating the presence of 2P, 3P, etc. harmonics in forcing due to shear.

We expect a 1P harmonic due to shear, but because it is not a pure sinusoid, there are
higher-order harmonics. However, the magnitude of these harmonics is small.

This script calculates a wind time series directly from the power-law shear profile and
then applies it to 3 dynamical systems with 3 different natural frequencies. As expected,
there is energy content in the higher harmonics, but it is very small and drops off
quickly. Thus, we don't expect shear to be a big problem with respect to natural
frequencies near 2P, 3P, etc.

Note, however, that the addition of turbulence is expected
to cause even more energy at higher frequencies due to rotational sampling. Thus, if
there is a natural frequency near 2P, 3P, etc., time-marching simulations should be run
to rule out potential resonance issues.

Author: Jenni Rinker
"""
import matplotlib.pyplot as plt
import numpy as np


#%% calculate u(t) from power law profile

# assumed values
Omega = 2*2 * np.pi  # rotational frequency [rad/s]
zref = 100  # hub height [m]
R = 50  # distance from hub height to wind location [m]
Uref = 10  # mean wind speed at hub height [m/s]
alpha = 0.2  # power-law shear parameter [-]
m = 10  # mass of blade, only used as a scaling parameter [kg]

# create time vector (assume we want 20 revolutions in our time vector)
T = 20 * (2 * np.pi) / Omega  # final time [s]
t = np.linspace(0, T, 500, endpoint=False)  # time vector [s]

# define the wind speed our blade sees, and assume that that directly correlates to forcing
z = zref + R * np.cos(Omega * t)  # blade location [m]
u = Uref * (z / zref) ** alpha  # wind speed at the blade [m/s]
beta = Uref / (zref**alpha)  # define this variable for convenience

plt.plot(t, u)

#%% calculate a few An using numerical integration of the Fourier series exponents

# functions to integrate
tint = np.linspace(0, 2*np.pi / Omega, 501)
uint = beta * (zref + R * np.cos(Omega * tint)) ** alpha

# first, the DC component. Verify versus theory
a0 = (Omega/np.pi) * np.trapz(uint, tint)
assert np.isclose(a0/2, u.mean())  # 1/2 a0 = mean(u), in theory

# define number of harmonics and initialize variables
nharm = 4  # how many N-P harmonics do we want to consider?
ans = np.empty(nharm)
bns = np.empty(nharm)
Ans = np.empty(nharm)
phis = np.empty(nharm)

# do the numeric integration
for i in range(nharm):
    ans[i] = (Omega/np.pi) * np.trapz(uint * np.cos((i+1)*Omega*tint), tint)
    bns[i] = (Omega/np.pi) * np.trapz(uint * np.sin((i+1)*Omega*tint), tint)
    Ans[i] = np.sqrt(ans[i]**2 + bns[i]**2)
    phis[i] = np.arctan2(bns[i], ans[i])

#%% for verification, determine An directly from FFT

# calculate FFT
f = np.fft.rfftfreq(u.size, t[1])
Xu = np.fft.rfft(u) / u.size
mags_onesided = 2 * np.abs(Xu)  # convert to one-sided magnitudes
Ans2 = np.empty(nharm)

# we have a large Fourier vector, so isolate values near 1P, 2P, up to nharm
for i in range(1, nharm+1):
    f_find = i * Omega / (2*np.pi)
    i_f = np.abs(f - f_find).argmin()
    Ans2[i-1] = mags_onesided[i_f]

#%% compare An values from numerical integration of Fourier series and FFT

print(f'\n{"ans":>11s}{"bns":>11s}{"Ans":>11s}{"phis":>11s}{"2|Xu[f]|":>11s}')
print('    ---------------------------------------------------')
for i in range(nharm):
    print(f'{ans[i]:11.4f}{bns[i]:11.4f}{Ans[i]:11.4f}{phis[i]:11.4f}{Ans2[i]:11.4f}')

# plot the components
fig, ax = plt.subplots(num=1, clear=True, figsize=(7, 3))
ax.stem(f[1:], mags_onesided[1:], basefmt=' ', linefmt='C1', markerfmt='C1D', label='Fourier transform')
ax.stem(np.arange(1, nharm+1)*Omega/(2*np.pi), Ans, basefmt=' ', label='Numerical integration')
ax.set(xlim=[0, (nharm+0.5)*Omega/(2*np.pi)], xlabel='Frequency [Hz]', ylabel='Sinusoid magnitude [m/s]')
plt.legend()
fig.tight_layout()

#%% calculate steady-state response magnitudes for 3 different natural freqs.
# ASSUME that the FRFs are scaled such that they have the same max value.
#   This is not actually true for a normal spring-mass-damper system.
# axes 0: harmonics in the wind. axes 1: the 3 FRFs. axes 2: response magnitude

zeta = 1 / np.sqrt(1 + (2*np.pi/0.03)**2)  # assuming 3% log dec damping on the blade
omegans = [Omega, 2*Omega, 3.5*Omega]

fig, axs = plt.subplots(3, 1, num=2, clear=True, figsize=(7, 6))
axs[0].plot(f[1:], np.abs(Xu)[1:])
axs[0].set(xlim=[0, (nharm+0.5)*Omega/(2*np.pi)])
axs[0].set_ylabel('Magnitude of\nwind [m/s]')

s = 1j * (2*np.pi*f)  # Lagrange variable s = jw
Xs = np.empty((f.size, len(omegans)), dtype=complex)
for i, omegan in enumerate(omegans):
    FRF = 1 / (s**2 + 2*zeta*omegan * s + omegan**2) / m
    FRF = FRF / np.abs(FRF).max()
    axs[1].plot(f[1:], np.abs(FRF)[1:], alpha=0.8, marker='x^o'[i], mfc='none',
                    label=f'$f_n$ = {omegan/(2*np.pi):.0f} Hz')
    axs[1].set(xlim=[0, (nharm+0.5)*Omega/(2*np.pi)])
    Xs[:, i] = FRF * Xu
    axs[2].plot(f[1:], np.abs(Xs[1:, i]), alpha=0.8, marker='x^o'[i], mfc='none',
                    label=f'$f_n$ = {omegan/(2*np.pi):.0f} Hz')
    axs[2].set(xlim=[0, (nharm+0.5)*Omega/(2*np.pi)])

axs[1].set_ylabel('Magnitude of\nFRF [m/(m/s)]')
axs[2].set_ylabel('Magnitude of\nresponse [m]')
axs[2].set(xlabel='Frequency [Hz]')
axs[1].legend(fontsize='small', loc=3)

fig.tight_layout()
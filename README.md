# Writeups about Loads, Aerodynamics and Control of Wind Turbines

This repository contains small handouts to investigate and explain different observations related to 
loads, aerodynamics and control of wind turbines

**List of write-ups**:  
 * [Why are the Fourier phases of the flapwise bending moment in a straight line at high
   frequencies?](https://gitlab.windenergy.dtu.dk/lac-course/lac-writeups/-/blob/main/phases-correlated-fbrm/why_are_fbrm_phases_correlated_at_high_frequencies.pdf)

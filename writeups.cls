%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LaTeX class for LAC course notes
% Written by rink, based on https://www.overleaf.com/learn/latex/Writing_your_own_class

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preliminaries
\NeedsTeXFormat{LaTeX2e}  % needed tex
\ProvidesClass{writeups}[2020/07/20 LaTeX class for 46320]  % class info
\LoadClass[12pt]{article}  % based on article class

% colors
\RequirePackage{xcolor}  % for title colors
\definecolor{dtured}{RGB}{153, 0, 0}  % dark red
\definecolor{dtugreen}{RGB}{0, 136, 53}  % dark green

\RequirePackage[utf8]{inputenc}  % input encoding
\RequirePackage{parskip}  % for paragraph skips
\RequirePackage[a4paper, margin=3cm]{geometry}  % set margins
\RequirePackage[colorlinks,allcolors=dtugreen]{hyperref}  % clickable links when referencing
\RequirePackage{graphicx}  % for figures

% set defaults
\newcommand{\headlinecolor}{\color{dtured}}  % header color
\renewcommand{\familydefault}{\sfdefault}  % sans serif

% allow breaking spaces in underscores (issues with margin)
\let\underscore\_
\renewcommand{\_}{\underscore\hspace{0pt}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define custom styles

% Title
\renewcommand{\maketitle}{%
	{\begin{flushleft}
        {\LARGE \bfseries \headlinecolor \@title \par} % title
        \vspace*{6pt}
        %{\large {\bfseries Course:} 46320 LAC Course (\@date) \par}  % date
        %\vspace*{-1ex}
        {\large {\bfseries Author(s):} \@author \par}  % contributors
        \vspace*{-1ex}
        {\large {\bfseries Date:} \@date \par}  % contributors
        \vspace*{4ex}
	\end{flushleft}}%
}

% Table of contents
\renewcommand{\tableofcontents}{%
  \subsection*{Contents:}\vspace*{3pt}
	\renewcommand*\l@section{\@dottedtocline{1}{1.5em}{2.3em}}  % section dots
  \setcounter{tocdepth}{1}  % only top level
  \@starttoc{toc}
  \vspace*{4ex}
}


% Section headers
\renewcommand{\section}{%
    \@startsection
    {section}{1}{0pt}%  name, level, indent
    	{-5ex plus -1ex minus -0.2ex}{1ex plus .2ex}%  before, after
    {\Large\bfseries\boldmath\headlinecolor}%  format
}

% Subsection headers
\renewcommand{\subsection}{
	\@startsection
	{subsection}{2}{\z@}%  name, level, indent
		{-16pt plus -1pt minus -3pt}{8pt}%  before, after
    {\reset@font\large\bfseries\boldmath\headlinecolor}%  format
}

% Subsubsection headers
\renewcommand{\subsubsection}{
	\@startsection{subsubsection}{3}{\z@}%  name, level, indent
		{-12pt plus -1pt minus -6pt}{-1em}%  before, after
    {\reset@font\normalsize\bfseries\boldmath\headlinecolor}%  format
}

% Figure names
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{{\bfseries\headlinecolor\sffamily#1:} #2}%
  \ifdim \wd\@tempboxa >\hsize
    {\bfseries\headlinecolor\sffamily#1:} #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
  
% remove parskip spacing in enumerate and itemize
\let\OLDenumerate\enumerate
\renewcommand\enumerate{\OLDenumerate\setlength{\parskip}{0pt}}
\let\OLDitemize\itemize
\renewcommand\itemize{\OLDitemize\setlength{\parskip}{0pt}}
